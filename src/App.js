import './App.css';
import{useState} from "react"
import{storage} from './firebase'
import{ref,uploadBytes,getDownloadURL} from'firebase/storage'
function App() {
  const [url, setUrl] = useState(null);

  const handleImageChange = (e) => {
    if (e.target.files[0]) {
      const imageRef = ref(storage, "image");
      uploadBytes(imageRef, e.target.files[0]).then(() => {
        getDownloadURL(imageRef).then((url) => {
          setUrl(url);
        }).catch(error => {
          console.log(error.message, "error getting image URL");
        });
      }).catch(error => {
        console.log(error.message);
      });
    }
  };

  console.log(url);

  return (
    <div className="App">
      {url && (
        <div>
          <img class="h-auto max-w-full rounded-lg" src={url} alt="" />
        </div>
      )}

      <input type='file' onChange={handleImageChange} />
    </div>
  );
}

export default App;
