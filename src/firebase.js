
import { initializeApp } from "firebase/app";
import { getStorage } from "firebase/storage";

const firebaseConfig = {
  apiKey: "AIzaSyDu6DSifAwfGTb_5_6y1_PjKnemqSJOblU",
  authDomain: "kbaenak.firebaseapp.com",
  projectId: "kbaenak",
  storageBucket: "kbaenak.appspot.com",
  messagingSenderId: "839797935319",
  appId: "1:839797935319:web:bd27d6b1095c54ac6f7492",
  measurementId: "G-VZ1C6NZ028"
};

const app = initializeApp(firebaseConfig);
export const storage=getStorage(app);